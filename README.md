# diy-s4h-scp
Do-It-Yourself SAP S/4HANA connected securely via SAP Cloud Connector with a Node.js application

## blog
[My blog on SAP Community walks you through the app](https://blogs.sap.com/2019/04/02/a-do-it-yourself-at-home-guide-how-to-connect-a-node.js-app-on-sap-cloud-platform-for-the-cloud-foundry-to-an-s4hana-on-premise-system-securely-via-cloud-connector/)

## .env
Modify `.env` file under `diy-app` folder. Add a real email address as a user ID to authenticate to SAP Cloud Platform.

This is used in `filterUser.js` in `diy-app` as a dummy substitute for a user database.
USER1 is for a customer who will see a list of bills -- mapped to S/4HANA customer 17100001. 
USER2 is for a prospect who has a SCP ID but not mapped to an S/4HANA customer.

## Notes on JavaScript Coding Style
I use JavaScript Standard Style; I have adjusted most of the source code from tutorials and other resources accordingly. Notably, statements in my code does not end with a semi-colon, “;”. 
